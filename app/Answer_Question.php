<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer_Question extends Model {

    protected $table = 'answer_questions';
    protected $fillable = ['question', 'status', 'id_seller', 'id_question'];

    public function question() {
        return $this->belongsTo('App/Question_Post');
    }

    public function seller() {
        return $this->hasOne('App/Seller');
    }

}
