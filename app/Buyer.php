<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model {

    protected $table = 'buyers';
    protected $fillable = ['date', 'status', 'id_user'];

    public function user() {
        return $this->hasOne('App\User');
    }
    
    public function purchase(){
        return $this->belongsTo('App\Purchase');
    }

    public function seller_review(){
        return $this->belongsTo('App\Seller_Review');
    }
    
}
