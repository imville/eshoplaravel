<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category_Image extends Model {

    protected $table = 'categorie_images';
    protected $fillable = ['name', 'status'];

    public function image() {
        return $this->belongsTo('App\Image');
    }

}
