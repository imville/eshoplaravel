<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ext_Images extends Model {

    protected $table = 'ext_images';
    protected $fillable = ['id','name','description','status'];

    public function image() {
        return $this->belongsTo('App\Image');
    }

}
