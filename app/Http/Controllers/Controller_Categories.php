<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class Controller_Categories extends Controller {

    public function index() {
        $list = Category::where('status', 1)->orderBy('name', 'ASC')->paginate(5);
        return view('admin.category.index')->with('all', $list);
    }

    public function create() {
        return view('admin.category.create');
    }

    public function store(Request $request) {
        $obj = new Category;
        $obj->fill($request->all());
        $obj->save();
        return redirect()->route('Categories.index');
    }

    public function show($id) {
        
    }

    public function edit($id) {
        $object = Category::find($id);
        return view('admin.category.edit')->with('obj', $object);
    }

    public function update(Request $request, $id) {
        $obj = Category::find($id);
        $obj->fill($request->all());
        $obj->save();
        return redirect()->route('Categories.index');
    }

    public function destroy($id) {
        $obj = Category::find($id);
        $obj->status = 0;
        $obj->save();
        return redirect()->route('Categories.index');
    }

}
