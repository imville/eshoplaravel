<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category_Image;

class Controller_Categories_Images extends Controller {

    public function index() {
        
        $list = Category_Image::where('status', 1)->orderBy('name', 'ASC')->get();
       return view('admin.category_Image.index')->with('all', $list);
    }

    public function create() {
        return view('admin.category_Image.create');
    }

    public function store(Request $request) {
        $object = new Category_Image;
        $object->fill($request->all());
        $object->save();
        return redirect()->route('Categories_Images.index');
    }

    public function show($id) {
        
    }

    public function edit($id) {
        $obj = Category_Image::find($id);

        return view('admin.category_Image.edit')->with('obj', $obj);
    }

    public function update(Request $request, $id) {
        $object =  Category_Image::find($id);
        $object->fill($request->all());
        $object->save();
         return redirect()->route('Categories_Images.index');
    }

    public function destroy($id) {
        $object = Category_Image::find($id);
        $object->status = 0;
        $object->save();
        return redirect()->route('Categories_Images.index');
    }

}
