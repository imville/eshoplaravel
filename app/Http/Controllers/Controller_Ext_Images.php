<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ext_Images;

class Controller_Ext_Images extends Controller {

    public function index() {
        $list_Ext_Images = Ext_Images::where('status', 1)->orderBy('name', 'ASC')->get();
        return view('admin.ext_Images.index')->with('allExtensions', $list_Ext_Images);
    }

    public function create() {
        return view('admin.ext_Images.create');
    }

    public function store(Request $request) {
        $ext_Image = new ext_Images;
        $ext_Image->name = $request->name;
        $ext_Image->description = $request->description;
        $ext_Image->save();
         return redirect()->route('Ext_Images.index');
    }

    public function show($id) {
        
    }

    public function edit($id) {
        $ext_Image = Ext_Images::find($id);

        return view('admin.ext_Images.edit')->with('extension', $ext_Image);
    }

    public function update(Request $request, $id) {
        $ext_Image = Ext_Images::find($id);
        $ext_Image->name = $request->name;
        $ext_Image->description = $request->description;
        $ext_Image->save();
        return redirect()->route('Ext_Images.index');
    }

    public function destroy($id) {
        $extImage = \App\Ext_Images::find($id);
        $extImage->status = 0;
        $extImage->save();
        return redirect()->route('Ext_Images.index');
    }

}
