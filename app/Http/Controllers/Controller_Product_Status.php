<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product_Status;

class Controller_Product_Status extends Controller {

    public function index() {
        $list = Product_Status::where('status', 1)->orderBy('name', 'ASC')->paginate(5);
        return view('admin.product_Status.index')->with('all', $list);
    }

    public function create() {
        return view('admin.product_Status.create');
    }

    public function store(Request $request) {
        $obj = new Product_Status;
        $obj->fill($request->all());
        $obj->save();
        return redirect()->route('Product_Status.index');
    }

    public function show($id) {
        
    }

    public function edit($id) {
        $object = Product_Status::find($id);
        return view('admin.product_Status.edit')->with('obj', $object);
    }

    public function update(Request $request, $id) {
        $obj = Product_Status::find($id);
        $obj->fill($request->all());
        $obj->save();
        return redirect()->route('Product_Status.index');
    }

    public function destroy($id) {
        $obj = Product_Status::find($id);
        $obj->status = 0;
        $obj->save();
        return redirect()->route('Product_Status.index');
    }

}
