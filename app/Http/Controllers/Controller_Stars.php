<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Star;

class Controller_Stars extends Controller {

    public function index() {
        $list = Star::where('status', 1)->orderBy('numberStars', 'ASC')->paginate(5);
        return view('admin.star.index')->with('all', $list);
    }

    public function create() {
        return view('admin.star.create');
    }

    public function store(Request $request) {
        $obj = new Star;
        $obj->fill($request->all());
        $obj->save();
        return redirect()->route('Stars.index');
    }

    public function show($id) {
        
    }

    public function edit($id) {
        $object = Star::find($id);
        return view('admin.star.edit')->with('obj', $object);
    }

    public function update(Request $request, $id) {
        $obj = Star::find($id);
        $obj->fill($request->all());
        $obj->save();
        return redirect()->route('Stars.index');
    }

    public function destroy($id) {
        $obj = Star::find($id);
        $obj->status = 0;
        $obj->save();
        return redirect()->route('Stars.index');
    }

}
