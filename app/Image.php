<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model {

    protected $table = 'images';
    protected $fillable = ['name', 'description', 'path', 'status', 'id_ext_image', 'id_categorie_image'];

    public function extension() {
        return $this->hasOne('App\Ext_Images', 'id');
    }

    public function category_image() {
        return $this->hasMany('App\Category_Image' , 'id');
    }

    public function product() {
        return $this->belongsTo('App\Product', 'id');
    }

}
