<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    protected $table = 'posts';
    protected $fillable = ['fecha', 'stock', 'status', 'id_seller', 'id_product'];

    public function sellers() {
        return $this->hasOne('App\Seller');
    }

    function product() {
        return $this->hasOne('App\Product');
    }
    
    public function purchases(){
        return $this->belongsTo('App\Purchase');
    }

}
