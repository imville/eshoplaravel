<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';
    protected $fillable = ['name', 'description', 'characteristics', 'status', 'id_image', 'id_category', 'id_product_status'];

    public function images() {
        return $this->hasMany('App\Image');
    }

    public function categories() {
        return $this->hasMany('App\Category');
    }

    public function status() {
        return $this->hasOne('App\Product_Status');
    }

    public function posts() {
        return $this->belongsTo('App\Post');
    }

    public function review(){
        return $this->belongsTo('App\Product_Review');
    }
}
