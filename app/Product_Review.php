<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Review extends Model {

    protected $table = 'product_reviews';
    protected $fillable = ['stars', 'description', 'status', 'id_product', 'id_user'];

    
    public function product(){
        return $this->hasMany('App\Product');
    }
    
    public function user(){
        return $this->hasOne('App\User');
    }
    
}
