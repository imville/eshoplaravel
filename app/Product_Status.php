<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_Status extends Model {

    protected $table = 'product_status';
    protected $fillable = ['name', 'status'];

    public function product() {
        return $this->belongsTo('App\Product');
    }

}
