<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model {

    protected $table = 'purchases';
    protected $fillable = ['date', 'status', 'id_post', 'id_buyer'];

    public function post() {
        return $this->hasMany('App/Post');
    }

    public function buyer() {
        return $this->hasMany('App/Buyer');
    }

}
