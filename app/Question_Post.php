<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question_Post extends Model {

    protected $table = 'question_posts';
    protected $fillable = ['question', 'date', 'status', 'id_buyer'];

    
    public function Answer_Question(){
        return $this->hasOne('App/Answer_Question');
    }
}
