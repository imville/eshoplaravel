<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model {

    protected $table = 'sellers';
    protected $fillable = ['DNI', 'id_user'];

    public function user() {
        return $this->hasOne('App\User');
    }

    public function posts() {
        return $this->belongsTo('App\Post');
    }

    public function seller_review() {
        return $this->belongsTo('App\Seller_Review');
    }

    public function answer_question(){
        return $this->belongsTo('App\Answer_Question');
    }
}
