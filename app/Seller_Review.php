<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller_Review extends Model {

    protected $table = 'seller_reviews';
    protected $fillable = ['stars_attention', 'stars_respone_time', 'description', 'status', 'id_seller', 'id_buyer'];

    public function seller() {
        return $this->hasMany('App\Seller');
    }

    public function buyer(){
        return $this->hasOne('App\Seller_Review');
    }
    
}
