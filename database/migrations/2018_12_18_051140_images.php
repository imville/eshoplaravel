<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Images extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description');
            $table->string('path');
            $table->unsignedInteger('id_ext_image')->unsigned();
            $table->unsignedInteger('id_categorie_image')->unsigned();
            $table->foreign('id_ext_image')->references('id')->on('ext_images')->onDelete('cascade');
            $table->foreign('id_categorie_image')->references('id')->on('categorie_images')->onDelete('cascade');

            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('images');
    }

}
