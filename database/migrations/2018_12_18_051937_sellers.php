<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sellers extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('DNI');
            $table->unsignedInteger('id_user');
            $table->foreign('id_user')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });


        Schema::create('sellers_products', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_seller');
            $table->unsignedInteger('id_product');
            $table->foreign('id_seller')->references('id')->on('sellers')->onDelete('cascade');
            $table->foreign('id_product')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('sellers');
    }

}
