<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_seller');
            $table->unsignedInteger('id_product');
            $table->dateTime('date');
            $table->integer('stock');
            $table->integer('status')->default(1);
            $table->foreign('id_seller')->references('id')->on('sellers')->onDelete('cascade');
            $table->foreign('id_product')->references('id')->on('products')->onDelete('cascade');

            $table->timestamps();
        });


        Schema::create('images_posts',function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('id_post');
            $table->unsignedInteger('id_image');
            $table->datetime('date');
            $table->integer('status')->default(1);
            $table->foreign('id_post')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('id_image')->references('id')->on('images')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('posts');
        Schema::dropIfExists('images_posts');

    }

}
