<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SellerReviews extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('seller_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_seller');
            $table->unsignedInteger('id_buyer');
            $table->integer('stars_attention');
            $table->integer('stars_respone_time');
            $table->string('description');
            $table->integer('status')->default(1);
            $table->foreign('id_seller')->references('id')->on('sellers')->onDelete('cascade');
            $table->foreign('id_buyer')->references('id')->on('buyers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('seller_reviews');
    }

}
