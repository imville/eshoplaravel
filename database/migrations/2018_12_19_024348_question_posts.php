<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QuestionPosts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
     * id_buyer int not null, 
      question varchar(1024) not null,
      date datetime not null default now(),
      status int not null default 1
     */
    public function up() {
        Schema::create('question_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_buyer');
            $table->unsignedInteger('id_post');
            $table->text('question');
            $table->datetime('date');
            $table->integer('status')->default(1);
            $table->foreign('id_buyer')->references('id')->on('buyers')->onDelete('cascade');
            $table->foreign('id_post')->references('id')->on('posts')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('question_posts');
    }

}
