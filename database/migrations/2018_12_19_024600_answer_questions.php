<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AnswerQuestions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('answer_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_seller')->unsigned();
            $table->unsignedInteger('id_question')->unsigned();
            $table->text('answer');
            $table->integer('status');
            $table->foreign('id_seller')->references('id')->on('sellers')->onDelete('cascade');
             $table->foreign('id_question')->references('id')->on('question_posts')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('answer_questions');
    }

}
