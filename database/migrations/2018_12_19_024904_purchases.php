<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Purchases extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    /*
     * create table purchases(
      id int not null auto_increment primary key unique,
      id_product int not null,
      id_seller int not null,
      id_user int not null,
      date datetime not null default now(),
      status int not null default 1
      )
     */
    public function up() {
        Schema::create('purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('id_post');
            $table->unsignedInteger('id_buyer');
            $table->datetime('date');
            $table->integer('status')->default(1);
            $table->foreign('id_post')->references('id')->on('posts')->onDelete('cascade');
            $table->foreign('id_buyer')->references('id')->on('buyers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('purchases');
    }

}
