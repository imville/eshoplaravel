
@extends('admin.template.main')

@section('title')

Crear una Extension para Imagenes

@stop


@section('nav')

@extends('admin.template.nav')

@stop

@section('mainContainer')


<div class="row">
    <h2>Categorias </h2>

</div>

{!! Form::open(['route' => ['Categories.update' , $obj->id] , 'method' =>'put']) !!}


<div class="form-group">
    {!!  Form::label('name', 'Nombre'); !!}
    {!!  Form::text('name' , $obj->name ,['class' => 'form-control' , 'placeholder' => 'JUEGOS']); !!}

</div>
<div class="form-group">

    {!! Form::submit('Guardar!' , ['class' => 'form-control btn btn-success'  ]); !!}
</div>
{!! Form::close()!!}

@stop

@section('footer')



@stop

