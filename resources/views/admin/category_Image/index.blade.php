@extends('admin.template.main')
@section('title')
Mostrar las extensiones guarda
@stop
@section('nav')
@extends('admin.template.nav')
@stop

@section('mainContainer')

<div class="row">
    <h2>Categorias de Imagenes</h2>

</div>
<div class="row">
    <div class="col-md-1 offset-md-11"><a class="btn btn-success" href="{{route('Categories_Images.create')}}">+</a></div>
    
    
</div>
<table class="table">
    <thead>
    <th>Id</th>
    <th>Name</th>
    <th>Actions</th>
</thead>
<tbody>
    @foreach($all as $obj)
    <tr>
        <td>{{ $obj->id }} </td>
        <td>{{ $obj->name }} </td>
        <td>
            <a href="{{ route('Categories_Images.destroyId', $obj->id)}}" class="btn btn-danger ">
                <span class="glyphicon glyphicon-search" aria-hidden="true">X</span>
            </a>
            <a href="{{ route('Categories_Images.edit', $obj->id) }}" class="btn btn-warning">
                 <span class="glyphicon glyphicon-search" aria-hidden="true">U</span>
            </a>
        </td>
    </tr>
    @endforeach
</tbody>

</table>



@stop