
@extends('admin.template.main')

@section('title')

Crear una Extension para Imagenes

@stop


@section('nav')

@extends('admin.template.nav')

@stop

@section('mainContainer')

{!! Form::open(['route' => 'Ext_Images.store']) !!}

<div class="row">
    <h2>Extensiones de imagenes</h2>

</div>

<div class="form-group">
    {!!  Form::label('name', 'Nombre'); !!}
    {!!  Form::text('name' , null,['class' => 'form-control' , 'placeholder' => 'JPG']); !!}

</div>
<div class="form-group">
    {!!  Form::label('description', 'Descripcion'); !!}
    {!!  Form::text('description' , null,['class' => 'form-control' , 'placeholder' => 'Imagenes']); !!}

</div>
<div class="form-group">

    {!! Form::submit('Guardar!' , ['class' => 'form-control btn btn-success'  ]); !!}
</div>
{!! Form::close()!!}

@stop

@section('footer')



@stop

