
@extends('admin.template.main')

@section('title')

Crear una Extension para Imagenes

@stop


@section('nav')

@extends('admin.template.nav')

@stop

@section('mainContainer')


<div class="row">
    <h2>Extensiones de imagenes</h2>

</div>

{!! Form::open(['route' => ['Ext_Images.update' , $extension->id] , 'method' =>'put']) !!}


<div class="form-group">
    {!!  Form::label('name', 'Nombre'); !!}
    {!!  Form::text('name' , $extension->name ,['class' => 'form-control' , 'placeholder' => 'JPG']); !!}

</div>
<div class="form-group">
    {!!  Form::label('description', 'Descripcion'); !!}
    {!!  Form::text('description' , $extension->description ,['class' => 'form-control' , 'placeholder' => 'Imagenes']); !!}

</div>
<div class="form-group">

    {!! Form::submit('Guardar!' , ['class' => 'form-control btn btn-success'  ]); !!}
</div>
{!! Form::close()!!}

@stop

@section('footer')



@stop

