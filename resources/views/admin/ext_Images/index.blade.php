@extends('admin.template.main')
@section('title')
Mostrar las extensiones guarda
@stop
@section('nav')
@extends('admin.template.nav')
@stop

@section('mainContainer')
<div class="row">
    <h2>Extensiones de imagenes</h2>

</div>

<div class="row">
    <div class="col-md-1 offset-md-11"><a class="btn btn-success" href="{{route('Ext_Images.create')}}">+</a></div>
    
    
</div>
<table class="table">
    <thead>
    <th>Id</th>
    <th>Name</th>
    <th>Descripcion</th>
    <th>Actions</th>
</thead>
<tbody>
    @foreach($allExtensions as $ext)
    <tr>
        <td>{{ $ext->id }} </td>
        <td>{{ $ext->name }} </td>
        <td>{{ $ext->description }} </td>
        <td>
            <a href="{{ route('Ext_Images.destroyId', $ext->id)}}" class="btn btn-danger ">
                <span class="glyphicon glyphicon-search" aria-hidden="true">X</span>
            </a>
            <a href="{{ route('Ext_Images.edit', $ext->id) }}" class="btn btn-warning">
                 <span class="glyphicon glyphicon-search" aria-hidden="true">U</span>
            </a>
        </td>
    </tr>
    @endforeach
</tbody>

</table>



@stop