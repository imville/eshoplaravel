@extends('admin.template.main')
@section('title')
Mostrar las extensiones guarda
@stop
@section('nav')
@extends('admin.template.nav')
@stop

@section('mainContainer')

<div class="row">
    <h2>Estado del Producto</h2>

</div>
<div class="row">
    <div class="col-md-1 offset-md-11"><a class="btn btn-success" href="{{route('Product_Status.create')}}">+</a></div>


</div>
<table class="table">
    <thead>
    <th>Id</th>
    <th>Name</th>
    <th>Actions</th>
</thead>
<tbody>
    @foreach($all as $obj)
    <tr>
        <td>{{ $obj->id }} </td>
        <td>{{ $obj->name }} </td>
        <td>
            <a href="{{ route('Product_Status.destroyId', $obj->id)}}" class="btn btn-danger ">
                <span class="glyphicon glyphicon-search" aria-hidden="true">X</span>
            </a>
            <a href="{{ route('Product_Status.edit', $obj->id) }}" class="btn btn-warning">
                <span class="glyphicon glyphicon-search" aria-hidden="true">U</span>
            </a>
        </td>
    </tr>
    @endforeach
</tbody>

</table>



@stop