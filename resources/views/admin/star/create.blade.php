
@extends('admin.template.main')

@section('title')

Crear una categoria de Imagenes

@stop


@section('nav')

@extends('admin.template.nav')

@stop

@section('mainContainer')

<div class="row">
    <h2>Estrellas</h2>

</div>

{!! Form::open(['route' => 'Stars.store']) !!}



<div class="form-group">
    {!!  Form::label('name', 'Nombre'); !!}
    {!!  Form::text('name' , null,['class' => 'form-control' , 'placeholder' => 'Excelente']); !!}

</div>
<div class="form-group">
    {!!  Form::label('numberStars', 'Numero de Estrellas'); !!}
    {!!  Form::text('numberStars' , null,['class' => 'form-control' , 'placeholder' => '5']); !!}

</div>
<div class="form-group">

    {!! Form::submit('Guardar!' , ['class' => 'form-control btn btn-success'  ]); !!}
</div>
{!! Form::close()!!}

@stop

@section('footer')



@stop

