
@extends('admin.template.main')

@section('title')

Crear una Extension para Imagenes

@stop


@section('nav')

@extends('admin.template.nav')

@stop

@section('mainContainer')


<div class="row">
    <h2>Estrellas </h2>

</div>

{!! Form::open(['route' => ['Stars.update' , $obj->id] , 'method' =>'put']) !!}


<div class="form-group">
    {!!  Form::label('name', 'Nombre'); !!}
    {!!  Form::text('name' , $obj->name ,['class' => 'form-control' , 'placeholder' => 'NUEVO']); !!}

</div>
<div class="form-group">
    {!!  Form::label('numberStars', 'Numero de Estrellas'); !!}
    {!!  Form::text('numberStars' , $obj->numberStars,['class' => 'form-control' , 'placeholder' => '5']); !!}

</div>
<div class="form-group">

    {!! Form::submit('Guardar!' , ['class' => 'form-control btn btn-success'  ]); !!}
</div>
{!! Form::close()!!}

@stop

@section('footer')



@stop

