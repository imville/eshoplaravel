<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => "admin"], function() {

    Route::resource('Ext_Images', 'Controller_Ext_Images');

    Route::get('Ext_Images/{id}/destroy', [
        'uses' => 'Controller_Ext_Images@destroy',
        'as' => 'Ext_Images.destroyId'
    ]);

    Route::resource('Categories_Images', 'Controller_Categories_Images');

    Route::get('Categories_Images/{id}/destroy', [
        'uses' => 'Controller_Categories_Images@destroy',
        'as' => 'Categories_Images.destroyId'
    ]);

    Route::resource('Categories', 'Controller_Categories');

    Route::get('Categories/{id}/destroy', [
        'uses' => 'Controller_Categories@destroy',
        'as' => 'Categories.destroyId'
    ]);

    Route::resource('Product_Status', 'Controller_Product_Status');

    Route::get('Product_Status/{id}/destroy', [
        'uses' => 'Controller_Product_Status@destroy',
        'as' => 'Product_Status.destroyId'
    ]);

    Route::resource('Stars', 'Controller_Stars');
    
    Route::get('Stars/{id}/destroy', [
        'uses' => 'Controller_Stars@destroy',
        'as' => 'Stars.destroyId'
    ]);
});

Route::group(['prefix' => "Images"], function() {

    Route::resource('Images', 'Controller_Images');
});

Route::group(['prefix' => "Products"], function() {

    Route::resource('Products', 'Controller_Products');
});

Route::group(['prefix' => "Answer_Questions"], function() {

    Route::resource('Answer_Questions', 'Controller_Answer_Questions');
});

Route::group(['prefix' => "Buyers"], function() {

    Route::resource('Buyers', 'Controller_Buyers');
});

Route::group(['prefix' => "Question_Posts"], function() {

    Route::resource('Question_Posts', 'Controller_Question_Posts');
});

Route::group(['prefix' => "Posts"], function() {

    Route::resource('Posts', 'Controller_Posts');
});

Route::group(['prefix' => "Product_Reviews"], function() {

    Route::resource('Product_Reviews', 'Controller_Product_Reviews');
});

Route::group(['prefix' => "Purchases"], function() {

    Route::resource('Purchases', 'Controller_Purchases');
});

Route::group(['prefix' => "Seller_Reviews"], function() {

    Route::resource('Seller_Reviews', 'Controller_Seller_Reviews');
});

Route::group(['prefix' => "Sellers"], function() {

    Route::resource('Sellers', 'Controller_Sellers');
});

Route::group(['prefix' => "Users"], function() {

    Route::resource('Users', 'Controller_Users');
});
